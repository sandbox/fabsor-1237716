<?php
/**
 * @file
 * uuid_node hooks on behalf of the file and image module.
 * The file and image module are very similar data-wise, so we
 * can use the same implementation for both.
 */

/**
 * Implements hook_uuid_node_features_export_alter().
 */
function file_uuid_node_features_export_alter(&$export, &$pipe, $node) {
  $fields = uuid_features_find_module_fields(array('image', 'file'), $node);
  foreach ($fields as $field_name => $field) {
    // Loop through all values of the field.
    foreach ($node->$field_name as $lang => $instances) {
      foreach ($instances as $instance) {
        if (!empty($instance['uuid'])) {
          $pipe['uuid_file'][$instance['uuid']] = $instance['uuid'];
        }
      }
    }
  }
}

/**
 * Implements hook_uuid_node_features_export_render_alter().
 */
function file_uuid_node_features_export_render_alter(&$export, $node, $module) {
  $fields = uuid_features_find_module_fields(array('image', 'file'), $node);
  foreach ($fields as $field_name => $field) {
    // Loop through all values of the field.
    foreach ($node->$field_name as $lang => $instances) {
      foreach ($instances as $delta => $instance) {
        $export->{$field_name}[$lang][$delta] = array(
          'uuid' => $instance['uuid'],
        );
      }
    }
  }
}

/**
 * Implements hook_uuid_node_features_rebuild_alter().
 * Replace filefield uuid's with a field array suitable for node_save().
 */
function file_uuid_node_features_rebuild_alter(&$node, $module) {
  $fields = uuid_features_find_module_fields(array('image', 'file'), $node);
  foreach ($fields as $field_name => $field) {
    // Loop through all values of the field.
    foreach ($node->$field_name as $lang => $instances) {
      foreach ($instances as $delta => $instance) {
        // Check if the file exists in the database.
        if ($file = uuid_file_features_load_object($instance['uuid'])) {
          $node->{$field_name}[$lang][$delta] = (array)$file;
        }
        else {
          // Unset the this field if we found no match, to avoid any problems
          // it might cause.
          unset($node->{$field_name}[$lang][$delta]);
        }
      }
    }
  }
}
