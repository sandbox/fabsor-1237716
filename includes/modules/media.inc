<?php

/**
 * @file
 * uuid_node hooks on behalf of the media module.
 */

/**
 * Implements hook_uuid_node_features_export_alter().
 */
function media_uuid_node_features_export_alter(&$export, &$pipe, $node) {
  $fields = uuid_features_find_module_fields(array('media'), $node);
  foreach ($fields as $field_name => $field) {
    // Loop through all values of the field.
    foreach ($node->$field_name as $lang => $instances) {
      foreach ($instances as $instance) {
        $uuid = uuid_file_features_find_uuid($instance['fid']);
        $pipe['uuid_file'][$uuid] = $uuid;
      }
    }
  }
}

/**
 * Implements hook_uuid_node_features_export_render_alter().
 */
function media_uuid_node_features_export_render_alter(&$export, $node, $module) {
  $fields = uuid_features_find_module_fields(array('media'), $node);
  foreach ($fields as $field_name => $field) {
    // Loop through all values of the field.
    foreach ($node->$field_name as $lang => $instances) {
      foreach ($instances as $delta => $instance) {
        $export->{$field_name}[$lang][$delta] = array(
          'uuid' => uuid_file_features_find_uuid($instance['fid']),
        );
      }
    }
  }
}

/**
 * Implements hook_uuid_node_features_rebuild_alter().
 * Replace filefield uuid's with a field array suitable for node_save().
 */
function media_uuid_node_features_rebuild_alter(&$node, $module) {
  $fields = uuid_features_find_module_fields(array('media'), $node);
  foreach ($fields as $field_name => $field) {
    foreach ($node->$field_name as $lang => $instances) {
      foreach ($instances as $delta => $instance) {
        if ($file = uuid_file_features_load_object($instance['uuid'])) {
          $node->{$field_name}[$lang][$delta]['fid'] = $file->fid;
        }
        else {
          // Unset the this field if we found no match, to avoid any problems
          // it might cause.
          unset($node->{$field_name}[$lang][$delta]);
        }
      }
    }
  }
}
