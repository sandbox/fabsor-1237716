<?php

/**
 * @file
 * Features hooks for the uuid_file features component.
 */

/**
 * Implements hook_features_export_options().
 */
function uuid_file_features_export_options() {
  $options = array();
  $result = db_select('file_managed', 'f')
    ->fields('f', array('filename', 'fid', 'uuid'))
    ->execute();
  foreach ($result as $file) {
    $options[$file->uuid] = t('@name', array(
      '@name' => $file->filename,
    ));
  }
  return $options;
}

/**
 * Implements hook_features_export().
 */
function uuid_file_features_export($data, &$export, $module_name = '') {
  $pipe = array();
  $map = features_get_default_map('uuid_file');
  $export['dependencies']['uuid_features'] = 'uuid_features';

  foreach ($data as $uuid) {
    if (isset($map[$uuid]) && $map[$uuid] != $module_name) {
      if (isset($export['features']['uuid_file'][$uuid])) {
        unset($export['features']['uuid_file'][$uuid]);
      }
      $export['dependencies'][$module] = $map[$uuid];
    }
    else {
      $export['features']['uuid_file'][$uuid] = $uuid;
    }
  }
  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function uuid_file_features_export_render($module, $data) {
  $translatables = $code = array();
  $code[] = '  $files = array();';
  $code[] = '';
  $fids = array();
  foreach ($data as $uuid) {
    $fid = uuid_file_find($uuid);
    if (!$fid) {
      continue;
    }
    $fids[] = $fid;
  }
  // Load all file objects.
  $files = file_load_multiple($fids);
  foreach ($files as $file) {
    // unset the file fid and the timestamp since it is changed for each
    // call to file_save.
    unset($file->fid, $file->timestamp);
    // Store the hash of the file to determine if it has been changed.
    $file->hash = file_exists($file->uri) ? md5_file($file->uri) : '';
    $file->extension = array_pop(explode('.', $file->filename));
    $file->module = $module;
    $code[] = '  $files[\'' . $file->uuid . '\'] = ' . features_var_export($file, '  ') . ';';
    if (!empty($translatables)) {
      $code[] = features_translatables_export($translatables, '  ');
    }
  }
  $code[] = '  return $files;';
  $code = implode("\n", $code);
  return array('uuid_features_default_files' => $code);
}

/**
 * Implements hook_features_revert().
 */
function uuid_file_features_revert($module) {
  uuid_file_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 * Rebuilds files based on UUID from code defaults.
 */
function uuid_file_features_rebuild($module) {
  $files = module_invoke($module, 'uuid_features_default_files');
  if (!empty($files)) {
    foreach ($files as $data) {
      uuid_file_features_save_file($data);
    }
  }
}

/**
 * Save a file from a features export.
 * @param mixed $file
 *   The file to save.
 */
function uuid_file_features_save_file($file) {
  $file = (object) $file;
  $source_dir = drupal_get_path('module', $file->module) . '/uuid_file';
  // Try to fetch another file with the same fid, and replace that file
  // if it already exists.
  $file->fid = uuid_file_find($file->uuid);
  $source_file = $source_dir . '/' . $file->uuid . '.' . $file->extension;
  // Copy the file from the module directory to our destination path,
  // but only if the file actually has changed.
  if (file_exists($source_file) && (!file_exists($file->uri) || md5_file($file->uri) != $file->hash)) {
    $file->uri = file_unmanaged_copy($source_file, $file->uri, FILE_EXISTS_REPLACE);
  }
  return file_save($file);
}

/**
 * Load a file object from code or from the database.
 * if the object is in code, it will be stored in the database and then returned,
 * since it is a faux exportable.
 * @param string uuid
 *   The uuid of the object.
 * @return object
 *   A fully populated file object.
 */
function uuid_file_features_load_object($uuid) {
  // Fetch all default files, so we can store them if they are not available yet.
  $default_files = &drupal_static(__FUNCTION__);
  // Check if the file exists in the database.
  $fid = uuid_file_find($uuid);
  if (!empty($fid)) {
    return file_load($fid);
  }
  // If we can't find the file in the database, we need to check if it's
  // defined in code. Let's save it if we find it.
  if (!$default_files) {
    $default_files = module_invoke_all('uuid_features_default_files');
  }
  if (isset($default_files[$uuid])) {
    return uuid_file_features_save_file($default_files[$uuid]);
  }
  return FALSE;
}

/**
 * Find a file uuid based on a fid.
 * @param int $fid
 *   the file id
 * @return
 *   the uuid of the file.
 */
function uuid_file_features_find_uuid($fid) {
  return db_select('file_managed', 'f')
    ->fields('f', array('uuid'))
    ->condition('fid', $fid)
    ->execute()
    ->fetchField();
}
